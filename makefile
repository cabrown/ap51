install:
	python -m pip install .
	mkdir -p ${HOME}/.config/ap51
	mkdir -p ${HOME}/.local/share/applications
	install ap51_conky_rc ${HOME}/.config/ap51/ap51_conky_rc
	install ap51_get_ip.py ${HOME}/.config/ap51/ap51_get_ip.py
	install ap51.desktop ${HOME}/.config/autostart/ap51.desktop
	install ap51.desktop ${HOME}/.local/share/applications/ap51.desktop

uninstall:
	-rm ${HOME}/.config/ap51/ap51_conky_rc
	-rm ${HOME}/.config/ap51/ap51_get_ip.py
	-rm ${HOME}/.config/autostart/ap51.desktop
	-rm -d ${HOME}/.config/ap51
	python -m pip uninstall -y ap51
