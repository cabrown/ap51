Description
-----------

AudioPlayer51 (``ap51``) is a minimalist music player with a textual 
user interface written in Python. It aims to provide a 
power-user-friendly interface with simple filelist and playlist control.

Instead of building an elaborate database of your music library,
``ap51`` allows you to quickly browse the filesystem and enqueue
files, directories, and playlists.

The original cplay was started by Ulf Betlehem in 1998 and is no longer
maintained.  ``cplay-ng`` is a rewrite that aimed to stay true to the 
original design while evolving with a shifting environment.

This is a fork of ``cplay-ng``, with a specific usecase. It ships with 
an associate conky script to display album art and track info. The idea 
is that the user will ssh in, using the curses interface to control 
the player, while metadata is shown on an attached display. Communication
between ap51 and the conky script occurs with tempfiles, which results in
an audioplayer that is simple, lightweight and robust.


.. image:: screenshot.png
   :alt: screenshot of ap51 with file browser

Requirements
------------

- `python3 <http://www.python.org/>`_
- `mpv <https://mpv.io/>`_
- `conky <https://github.com/brndnmtthws/conky>`_

Installation
------------

::

    $ make install

Usage
-----

::

    $ ap51

Press ``h`` to get a list of available keys.

Issues
------

- Volume control does not work. This is because I switched to the alsa backend 
    in mpv in order to access a DAC, and I haven't gotten amixer to work just 
    yet. This is not a major issue for me though. 

Future Directions
-----------------

- The backend will eventually be switched to medussa <https://github.com/cbrown1/medussa>
    which is much more Pythonic, and will allow much more control.

