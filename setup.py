#!/usr/bin/env python

from setuptools import setup

README = open('README.rst').read()

setup(
    name='ap51',
    version='1.0',
    description='AudioPlayer51: A simple curses-based audio player',
    long_description=README,
    url='https://gitlab.com/cabrown/ap51',
    author='Christopher Brown',
    author_email='cabrown149@gmail.com',
    maintainer='Christopher Brown',
    maintainer_email='cabrown149@gmail.com',
#    data_files=[('bitmaps', ['bm/b1.gif', 'bm/b2.gif']),
#                  ('config', ['cfg/data.cfg'])],
    py_modules=['ap51'],
    #packages=['ap51'],
    entry_points={'console_scripts': [
        'ap51=ap51:main',
    ]},
    license='GPLv2+',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console :: Curses',
        'Intended Audience :: End Users/Desktop',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'License :: OSI Approved :: GNU General Public License v2 or later '
            '(GPLv2+)',
        'Topic :: Multimedia :: Sound/Audio :: Players',
    ],
)
